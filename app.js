const express = require('express');
var bodyParser = require('body-parser');
const app = express();
const db = require('./db');

const path = require('path');
const publicPath = path.join(__dirname, './public');

app.use(bodyParser.urlencoded({ extended: true }))
app.set('view engine', 'ejs')
app.use(express.static(publicPath));
app.use('/', require('./routes'));
module.exports = app;