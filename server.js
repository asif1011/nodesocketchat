const app = require('./app');
const port = process.env.PORT || 3000;
const generateMessage = require('./utils/generateMessage');
var room = 'grappus'
var http = require('http');

let server = app.listen(port, function() {
  console.log('Express server listening on port ' + port);
});

const io = require('socket.io').listen(server);

const Users = require('./utils/users');

var users = new Users(); 

io.on('connection', function (socket) {

  socket.on('registerUser', function (data) {

    if (data.userId && data.userName){
      socket.join(room)
      users.removeUser(socket.id);
      users.addUser(socket.id, data.userId, data.userName, room);
      io.to(room).emit('updateUserList', users.getUserList(room));
    }
  });

  socket.on('privateChat', function (data) {
    if (data.chatRoomObj) {
      socket.join(data.chatRoomObj._id)
    }
  });

  socket.on('typing', function (data) {
    io.to(room).emit('showUserTyping', data);
  });

  socket.on('stopTyping', function (data) {
    io.to(room).emit('stopUserTyping', data);
  });

  socket.on('createMessage', async function (data) {
    let newData = await generateMessage(data)
    io.to(data.chatRoomId).emit('newMessage', newData);
    io.to(room).emit('messageNotification', newData);
  });

  socket.on('disconnect', function () {
    let offlineUser = users.getUser(socket.id)
    users.removeUser(socket.id);
    io.to(room).emit('showOffline', offlineUser)
  });
});