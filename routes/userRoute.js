const express = require('express')
const router = express.Router()
const userController = require('./../controllers/userController');

router.get('/', userController.homePage);
router.post('/login', userController.login);
router.post('/getChatRoomId', userController.getChatRoomId);
module.exports = router;
