const userModel = require('./../models/userModel');
const chatRoomModel = require('./../models/chatRoomModel');
const messageModel = require('./../models/messageModel');


let homePage = async function(request, response){
  let userList = await userModel.find({})
  return response.render(__dirname + "./../views/home", { userList: userList })
}

let login = async function(request, response){
  try {
    if (request.body.userName.trim() === ""){
      return response.status(500).send({status: false, error: error})
    }
    let userObj = await userModel.findOneAndUpdate({userName: request.body.userName}, request.body, {upsert:true, new: true});
    data = {
      status: true,
      userObj: userObj,
    }
    return response.status(200).send(data);
  } catch (error) {
    return response.status(500).send({status: false, error: error})
  }
}

let getChatRoomId = async function (request, response) {

  const firstUserId = request.body.firstUserId;
  const secondUserId = request.body.secondUserId;
  const firstPossibleId = firstUserId + secondUserId;
  const secondPossibleId = secondUserId + firstUserId;
  const possibleChatIds = [firstPossibleId, secondPossibleId]
  try {
    if (firstUserId === secondUserId){
      return response.status(200).send({status: false, error: false, message: 'Nope! khud se chat ni kr sakte'})
    }
    let chatRoomObj = await chatRoomModel.findOne({chatId: {$in: possibleChatIds}});
    if(chatRoomObj){
      let messageList = await messageModel.find({chatRoom: chatRoomObj._id}).populate('sender receiver').sort('createdAt').limit(50)
      return response.status(200).send({chatRoomObj: chatRoomObj, status: true, messageList: messageList});
    }else{
      let data = {
        chatId: firstUserId + secondUserId,
        users: [firstUserId, secondUserId]
      }
      let chatRoomObj = await chatRoomModel.create(data);
      return response.status(200).send({chatRoomObj: chatRoomObj, status: true, messageList: []});
    }
  } catch (error) {
    return response.status(500).send({status: false, error: error})
  }
}

module.exports = {
  homePage,
  login,
  getChatRoomId,
};