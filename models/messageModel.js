const mongoose = require('mongoose');
const SCHEMA = mongoose.Schema;

var messageSchema = new mongoose.Schema({
  chatRoom: {
    type: SCHEMA.Types.ObjectId,
    ref: 'chatRoom'
  },
  sender: {
    type: SCHEMA.Types.ObjectId,
    ref: 'User'
  },
  receiver: {
    type: SCHEMA.Types.ObjectId,
    ref: 'User'
  },
  message: {
    type: String,
  },
  createdAt: {
    type: Number
  }
});

let messageModel = mongoose.model('Message', messageSchema);

module.exports = messageModel;