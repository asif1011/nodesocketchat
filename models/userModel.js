var mongoose = require('mongoose');

var userSchema = new mongoose.Schema({
    userName: {
        type: String,
        unique: true,
        index: true,
        required: 'UserName can not be left empty'
    }
});

let userModel = mongoose.model('User', userSchema);

module.exports = userModel;