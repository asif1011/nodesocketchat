const mongoose = require('mongoose');
const SCHEMA = mongoose.Schema;

let arrayLengthValidator = function (min, max) {
    return array => array.length <= max && array.length >= min;
};

var chatRoomSchema = new mongoose.Schema({
    chatId: {
        type: String,
        unique: true,
        index: true,
    },
    users: {
        type: [{
            type: SCHEMA.Types.ObjectId,
            ref: 'User',
        }], 
        validate: [arrayLengthValidator(2, 2), 'Chat can only be done between two users']
    }
});

let chatRoomModel = mongoose.model('chatRoom', chatRoomSchema);

module.exports = chatRoomModel;