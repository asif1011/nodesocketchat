class Users {
  constructor() {
    this.users = [];
  }

  addUser(socketId, userId, userName, room) {
    var user = {socketId, userId, userName, room };
    this.users.push(user);
    return user;
  }

  removeUser(socketId) {
    var user = this.getUser(socketId);
    if (user) {
      this.users = this.users.filter((user) => user.socketId !== socketId);
    }
    return user;
  }

  getUser(socketId) {
    return this.users.filter((user) => user.socketId === socketId)[0]
  }

  getUserList(room) {
    var users = this.users.filter((user) => {
      return user.room === room
    });
    var usersArray = users.map((user) => {
      return user
    });

    return usersArray;
  }
}

module.exports = Users;