const messageModel = require('./../models/messageModel');

let generateMessage = async function(data) {

  const body = {
    chatRoom: data.chatRoomId,
    sender: data.userId,
    receiver: data.secondUserId,
    message: data.message,
    createdAt: Date.now()
  }
  let messageObj = await messageModel.create(body);
  let newMessageObj = await messageModel.findById(messageObj._id).populate('sender receiver')
  return newMessageObj
};

module.exports = generateMessage;